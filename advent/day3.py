from dataclasses import dataclass
from enum import Enum
from typing import List, Tuple


class PositionStatus(Enum):
    OPEN = "."
    TREE = "#"


@dataclass
class Position:
    x: int
    y: int
    status: PositionStatus


@dataclass
class Slope:
    horizontal: int
    vertical: int


@dataclass
class TraversedPath:
    statuses: List[PositionStatus]

    @property
    def tree_count(self) -> int:
        return sum([s == PositionStatus.TREE for s in self.statuses])


@dataclass
class Tile:
    string_tile: str

    def __post_init__(self):
        self.grid: List[str] = [line for line in self.string_tile.split() if line]
        self.width = len(self.grid[0])
        self.height = len(self.grid) - 1

    def traverse(self, slope: Slope) -> TraversedPath:
        current_x = 0
        current_y = 0

        statuses = []

        while current_y < self.height:
            current_x = (current_x + slope.horizontal) % self.width
            current_y += slope.vertical
            statuses.append(self[current_x, current_y])

        return TraversedPath(statuses)

    def __getitem__(self, pos: Tuple[int, int]) -> PositionStatus:
        x, y = pos
        obj = self.grid[y][x]
        status = PositionStatus(obj)
        return status
