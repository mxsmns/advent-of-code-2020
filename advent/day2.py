from __future__ import annotations

import re
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Sequence


@dataclass
class TwoFieldPassword(ABC):
    password_string: str
    character: str
    field_a: int
    field_b: int

    @abstractmethod
    def is_valid(self) -> bool:
        ...

    @classmethod
    def from_string(cls, raw_string: str) -> TwoFieldPassword:
        matches = re.match(
            r"^(?P<field_a>\d+)-(?P<field_b>\d+)\s+(?P<character>\w+):\s+(?P<password>.*)$",
            raw_string,
        )
        if matches is None:
            raise ValueError("Unexpected format")

        password = cls(
            matches["password"],
            matches["character"],
            int(matches["field_a"]),
            int(matches["field_b"]),
        )

        return password

    @classmethod
    def count_valid_passwords_in_list(cls, raw_strings: Sequence[str]) -> int:
        num_valid_passwords = 0

        for raw_string in raw_strings:
            password = cls.from_string(raw_string)
            if password.is_valid():
                num_valid_passwords += 1

        return num_valid_passwords


class OccurancePassword(TwoFieldPassword):
    def is_valid(self) -> bool:
        return (
            self.field_a <= self.password_string.count(self.character) <= self.field_b
        )


class PositionalPassword(TwoFieldPassword):
    def is_valid(self) -> bool:
        return (self.password_string[self.field_a - 1] == self.character) != (
            (self.password_string[self.field_b - 1] == self.character)
        )
