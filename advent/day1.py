from __future__ import annotations

import math
from dataclasses import dataclass
from itertools import combinations
from typing import Iterator, List, Protocol, Tuple


class ExpenseCombination(Protocol):
    def is_valid(self) -> bool:
        ...

    def validated_value(self) -> int:
        ...


@dataclass
class IntegerCombination:
    values: Tuple[int, ...]

    def sum(self) -> int:
        return sum(self.values)

    def combine(self) -> int:
        return math.prod(self.values)


class IntegerCombination2020(IntegerCombination):
    def is_valid(self) -> bool:
        return self.sum() == 2020

    def validated_value(self) -> int:
        return self.combine()


@dataclass
class ExpenseReport:
    raw_values: List[int]

    def find_single_valid_pair(self) -> int:
        return self.find_single_valid_combination(2)

    def find_single_valid_triplet(self) -> int:
        return self.find_single_valid_combination(3)

    def find_single_valid_combination(self, combination_size: int) -> int:
        for combination in self._find_all_combinations(combination_size):
            if combination.is_valid():
                return combination.validated_value()

        raise ValueError("No valid pairs found")

    def _find_all_combinations(
        self, combination_size: int
    ) -> Iterator[ExpenseCombination]:
        for combination in combinations(self.raw_values, combination_size):
            yield IntegerCombination2020(combination)
