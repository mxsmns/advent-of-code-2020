from pathlib import Path

import pytest
from advent.day2 import OccurancePassword, PositionalPassword


@pytest.mark.parametrize(  # type: ignore
    "password,character,min_occurances,max_occurances,is_valid",
    [
        ("abcde", "a", 1, 3, True),
        ("cdefg", "b", 1, 3, False),
        ("ccccccccc", "c", 2, 9, True),
    ],
)  # type: ignore
def test_character_occurance_password_policy(
    password: str,
    character: str,
    min_occurances: int,
    max_occurances: int,
    is_valid: bool,
):
    password_under_test = OccurancePassword(
        password, character, min_occurances, max_occurances
    )
    assert password_under_test.is_valid() == is_valid


def test_load_occurance_password_from_string():
    input_string = "4-5 l: rllllj"
    password = OccurancePassword.from_string(input_string)

    assert password == OccurancePassword("rllllj", "l", 4, 5)


@pytest.mark.parametrize(  # type: ignore
    "password,character,min_occurances,max_occurances,is_valid",
    [
        ("abcde", "a", 1, 3, True),
        ("cdefg", "b", 1, 3, False),
        ("ccccccccc", "c", 2, 9, False),
    ],
)  # type: ignore
def test_character_positional_password_policy(
    password: str,
    character: str,
    min_occurances: int,
    max_occurances: int,
    is_valid: bool,
):
    password_under_test = PositionalPassword(
        password, character, min_occurances, max_occurances
    )
    assert password_under_test.is_valid() == is_valid


def test_load_positional_password_from_string():
    input_string = "4-5 l: rllllj"
    password = PositionalPassword.from_string(input_string)

    assert password == PositionalPassword("rllllj", "l", 4, 5)


def test_part_1():
    input_file = Path(__file__).parent / "inputs" / "day2.txt"

    raw_strings = input_file.read_text().splitlines()
    assert OccurancePassword.count_valid_passwords_in_list(raw_strings) == 434


def test_part_2():
    input_file = Path(__file__).parent / "inputs" / "day2.txt"

    raw_strings = input_file.read_text().splitlines()
    assert PositionalPassword.count_valid_passwords_in_list(raw_strings) == 509
