from pathlib import Path

import pytest
from advent.day3 import PositionStatus, Slope, Tile, TraversedPath


def test_tile_creation(puzzle_tile: Tile):
    assert puzzle_tile.grid == [
        "..##.......",
        "#...#...#..",
        ".#....#..#.",
        "..#.#...#.#",
        ".#...##..#.",
        "..#.##.....",
        ".#.#.#....#",
        ".#........#",
        "#.##...#...",
        "#...##....#",
        ".#..#...#.#",
    ]


@pytest.mark.parametrize("x,y,expected_object", [(3, 1, PositionStatus.OPEN), (3, 0, PositionStatus.TREE)])  # type: ignore
def test_get_object_at_position(
    x: int, y: int, expected_object: PositionStatus, puzzle_tile: Tile
):
    assert puzzle_tile[x, y] == expected_object


def test_traverse_straight_down(puzzle_tile: Tile):
    expected_result = TraversedPath(
        [
            PositionStatus.TREE,
            PositionStatus.OPEN,
            PositionStatus.OPEN,
            PositionStatus.OPEN,
            PositionStatus.OPEN,
            PositionStatus.OPEN,
            PositionStatus.OPEN,
            PositionStatus.TREE,
            PositionStatus.TREE,
            PositionStatus.OPEN,
        ]
    )

    assert (
        puzzle_tile.traverse(slope=Slope(horizontal=0, vertical=1)) == expected_result
    )


def test_traverse_three_one(puzzle_tile: Tile):
    expected_result = TraversedPath(
        [
            PositionStatus.OPEN,
            PositionStatus.TREE,
            PositionStatus.OPEN,
            PositionStatus.TREE,
            PositionStatus.TREE,
            PositionStatus.OPEN,
            PositionStatus.TREE,
            PositionStatus.TREE,
            PositionStatus.TREE,
            PositionStatus.TREE,
        ]
    )

    assert (
        puzzle_tile.traverse(slope=Slope(horizontal=3, vertical=1)) == expected_result
    )


@pytest.fixture
def puzzle_tile() -> Tile:
    tile = Tile(
        """
        ..##.......
        #...#...#..
        .#....#..#.
        ..#.#...#.#
        .#...##..#.
        ..#.##.....
        .#.#.#....#
        .#........#
        #.##...#...
        #...##....#
        .#..#...#.#
        """
    )
    return tile


@pytest.mark.integration
def test_part_1():
    input_file = Path(__file__).parent / "inputs" / "day3.txt"
    tile_string = input_file.read_text()
    tile = Tile(tile_string)
    path = tile.traverse(Slope(3, 1))
    path.tree_count == 237


@pytest.mark.integration
def test_part_2():
    input_file = Path(__file__).parent / "inputs" / "day3.txt"
    tile_string = input_file.read_text()

    tile = Tile(tile_string)
    slopes = [Slope(1, 1), Slope(3, 1), Slope(5, 1), Slope(7, 1), Slope(1, 2)]

    tree_counts = [tile.traverse(slope).tree_count for slope in slopes]

    total_tree_count = 1
    for count in tree_counts:
        total_tree_count *= count

    assert total_tree_count == 2106818610
