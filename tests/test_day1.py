from pathlib import Path

import pytest
from advent import day1


def test_day1_find_single_valid_pair():
    expense_report_values = [1721, 979, 366, 299, 675, 1456]

    report = day1.ExpenseReport(expense_report_values)

    assert report.find_single_valid_pair() == 514579


def test_day1_find_single_valid_pair_none_found():
    expense_report_values = [1, 2, 3, 4]

    report = day1.ExpenseReport(expense_report_values)

    with pytest.raises(ValueError):
        report.find_single_valid_pair()


@pytest.mark.integration
def test_day_1():
    input_file = Path(__file__).parent / "inputs" / "day1.txt"
    expense_report_string_values = input_file.read_text().splitlines()
    expense_report_values = [int(value) for value in expense_report_string_values]

    report = day1.ExpenseReport(expense_report_values)

    assert report.find_single_valid_pair() == 776064
    assert report.find_single_valid_triplet() == 6964490
